//Mock Database
let posts = [];

let count = 1;

//Add Post Data        from index.html                             event
document.querySelector('#form-add-post').addEventListener('submit', (e) => {

	e.preventDefault();

	posts.push({

		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value

	});

	console.log(count)
	count++;
	console.log(count)

	showPosts(posts)
	alert('Successfully added.')


})


const showPosts = (posts) => {

	let postEntries = '';

	posts.forEach((post) => {

		postEntries += `
			<div id="post-${post.id}" > 
				<h3 id="post-title-${post.id}"> ${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
														
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	})
	// 	     method                             property
	document.querySelector("#div-post-entries").innerHTML = postEntries;


}

//Edit Post

const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;


	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

}


//Update Post                                                               
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {

	e.preventDefault();

	for (let i = 0; i < posts.length; i++) {
        //From Array                        from edit form
		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {


			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

            //invoke 
			showPosts(posts)
			alert("Successfully updated");

            //para ma-end yung iteration
			break;
		}

	}

})



// // DELETE POST                                                             
// const deletePost = (post.id) => {
//    deletePost.remove();

// document.querySelector('#div-post-entries').addEventListener('submit', function() {
//     // console.log("you double clicked the button")
//     document.querySelector('#div-post-entries').classList.remove('#div-post-entries'); 
//     // console.log(document.querySelector("#headingText").classList)
// })

//    

// Delete
// document.querySelector('#div-post-entries').addEventListener('submit', (e) => {

// const deletePost = (id) => {
// 	document.getElementById('#div-post-entries').innerHTML = ''
// }   

// const deletePost = (posts) => {

// 	let deletePostEntries = '';
// 	const post1 = 
// 	document.querySelector('#post-1')
	       
// 	document.removeChild('#div-post-entries').innerHTML = deletePostEntries

// }

const deletePost = (id) => {
	posts = posts.filter((post) => {
		if(post.id.toString() !== id){
			console.log(post)
			return post
		}
	});

	document.querySelector(`#post-${id}`).remove();

	console.log(posts)
};



